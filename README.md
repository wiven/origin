# Origin CMS


The CMS for clients of WiVen Web Solutions (originally a fork of [Cockpit CMS](http://getcockpit.com)). Add content management functionality to any site - plug & play CMS.
Manage content like collections, regions, forms and galleries which you can reuse anywhere on your website.


![Origin CMS](https://wiven.be/origin/teaser.png)


* Homepage: [https://wiven.be/origin](https://wiven.be/origin)
* Twitter: [@wivenweb](https://twitter.com/wivenweb)


### Requirements

* PHP >= 5.4
* PDO + SQLite (or MongoDB)
* GD extension

make also sure that ```$_SERVER['DOCUMENT_ROOT']``` exists and is set correctly.


### Installation

1. Download Origin and put the origin folder in the root of your web project
2. Make sure that the __/cockpit/storage__ folder and all its subfolders are writable
3. Go to __/cockpit/install__ via Browser
4. You're ready to use Origin CMS :-)

### Usage

**Embed Origin**

Embedding Origin is really easy. Just include the following snippet anywhere you want to use Origin:

```php
// make origin api available
require('path/to/origin/bootstrap.php');
```

**Regions**

Render regions api:

```html
<div><?php region("address") ?></div>
<div><?=get_region("address") ?></div>
```

**Collections**

Loop over collection data:

```html
<?php foreach(collection("posts")->find(["active"=>1]) as $post): ?>
    <div class="post">
        <h3><?=$post["title"];?></h3>
        <p>
            <?=$post["content"];?>
        </p>
    </div>
<?php endforeach; ?>
```

### Documentation

Please visit http://getcockpit.com/docs


### Language files

Please visit and contribute to https://github.com/aheinze/cockpit-i18n

### Support

Developer: [Wim Vandevenne](mailto:info@wiven.be)


### Copyright and license

Copyright 2016 [WiVen Web Solutions](https://wiven.be) under the MIT license.

The MIT License (MIT)

Copyright (c) 2016 WiVen Web Solutions, https://wiven.be

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.