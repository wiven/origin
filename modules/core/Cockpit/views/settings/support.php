
<h1><a href="@route('/settingspage')">@lang('Settings')</a> / @lang('Support')</h1>

<div class="uk-grid" data-uk-grid-margin>

    <div class="uk-width-medium-1-1">
        <div class="app-panel">
            <h1>@lang('Need support?')</h1>

            <div>
                <img src="https://wiven.be/logo.png" width="100%" style="margin-bottom: 2.5em; max-width: 350px; display: block;">
                <a href="mailto:info@wiven.be"><i class="uk-icon-envelope-o"></i> info@wiven.be</a><br>
                <a href="tel:+32495741421"><i class="uk-icon-phone"></i> +32 495 74 14 21</a><br>
                <a href="https://wiven.be" target="_blank"><i class="uk-icon-link"></i> wiven.be</a>
            </div>
        </div>
    </div>

</div>
